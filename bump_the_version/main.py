#!/usr/bin/env python3
## @todo(stdmatt): Add the license header!
##


##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
import os;
import os.path;
import sys;
import json;
import re;

##----------------------------------------------------------------------------##
## Info                                                                       ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
PROGRAM_NAME            = "bump-the-version";
PROGRAM_VERSION         = "2.0.0";
PROGRAM_AUTHOR          = "stdmatt - <stdmatt@pixelwizads.io>";
PROGRAM_COPYRIGHT_OWNER = "arkadia games";
PROGRAM_COPYRIGHT_YEARS = "2021";
PROGRAM_DATE            = "19 November 2021";
PROGRAM_LICENSE         = "GPLv3";

##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
ARK_BUMP_DOT_FILENAME = ".ark_bump";

COMP_MAJOR = "major";
COMP_MINOR = "minor";
COMP_BABY  = "baby";


##----------------------------------------------------------------------------##
## Helper Functions                                                           ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def show_help(exit_code = 0):
    msg = """Usage:
  {program_name} [--help] [--version] [--create]
  {program_name} <major | minor | baby> [path]

Options:
    *--help    : Show this screen.
    *--version : Show program version and copyright.

    *--create : Create an ({dot_filename}) template in the current directory.

Arguments:
    major : bumps X.0.0 - 1.2.4  -> 2.0.0
    minor : bumps x.Y.0 - 1.4.8  -> 1.5.0
    baby  : bumps x.y.Z - 1.2.10 -> 1.2.11

    [path] : Path to ({dot_filename}) or a directory containing this file.

Notes:
  Options marked with * are exclusive, i.e. the {program_name} will run that
  and exit after the operation.
""";

    print(msg.format(
        program_name=PROGRAM_NAME,
        dot_filename=ARK_BUMP_DOT_FILENAME
    ));
    exit(exit_code);

##------------------------------------------------------------------------------
def show_version():
    msg = """{program_name} - {program_version} - {program_author}
Copyright (c) {program_copyright_years} - {program_copyright_owner}
This is a free software ({program_license}) - Share/Hack it
Check http://stdmatt.com for more :)""".format(
        program_name=PROGRAM_NAME,
        program_version=PROGRAM_VERSION,
        program_author=PROGRAM_AUTHOR,
        program_copyright_years=PROGRAM_COPYRIGHT_YEARS,
        program_copyright_owner=PROGRAM_COPYRIGHT_OWNER,
        program_license=PROGRAM_LICENSE,
    );
    print(msg);
    exit(0);


##------------------------------------------------------------------------------
def log_fatal(fmt, *args):
    print("[FATAL] {0}".format(
        fmt.format(*args)
    ));
    exit(1);

##------------------------------------------------------------------------------
def find_component_to_bump(args):
    for arg in args:
        if(arg == COMP_MAJOR):
            return COMP_MAJOR;
        if(arg == COMP_MINOR):
            return COMP_MINOR;
        if(arg == COMP_BABY):
            return COMP_BABY;

    log_fatal(
        "Invalid version component to bump - Expected: ({})",
        [COMP_MAJOR, COMP_MINOR, COMP_BABY]
    );

##------------------------------------------------------------------------------
def find_dot_bump_fullpath(args):
    bump_dirpath = None;
    for path in args:
        if(os.path.isfile(path)):
            return path;
        if(os.path.isdir(path)):
            bump_dirpath = path;
            break;

    if(bump_dirpath is None):
        bump_dirpath = os.path.abspath(".");

    bump_filepath = os.path.join(bump_dirpath, ARK_BUMP_DOT_FILENAME);
    if(os.path.isfile(bump_filepath)):
        return bump_filepath;
    else:
        log_fatal("Couldn't find ({}) file in path: ({})", ARK_BUMP_DOT_FILENAME, path);

##------------------------------------------------------------------------------
def bump(old_version, action):
    version_components = old_version.split(".")
    if(len(version_components) != 3):
        log_fatal("Version is wrong format: ({}) - Expecting: Major.Minor.Baby", old_version);

    if(action == COMP_MAJOR):
        version_components[0] = int(version_components[0]) + 1;
        version_components[1] = 0;
        version_components[2] = 0;

    elif(action == COMP_MINOR):
        version_components[1] = int(version_components[1]) + 1;
        version_components[2] = 0;

    elif(action == COMP_BABY):
        version_components[2] = int(version_components[2]) + 1;

    new_version = "{0}.{1}.{2}".format(
        version_components[0],
        version_components[1],
        version_components[2]
    );

    return new_version;

##------------------------------------------------------------------------------
def create_dot_file():
    template = """[
        {
            "name": "Main Version",
            "file": "./bump_the_version/main.py",
            "content": "PROGRAM_VERSION = \"_VERSION_\""
        }
    ]""";

    path_to_create = os.path.abspath(os.path.join(".", ARK_BUMP_DOT_FILENAME));
    if(os.path.isfile(path_to_create)):
        log_fatal("Already have ({}) at ({})", ARK_BUMP_DOT_FILENAME, path_to_create);

    f = open(path_to_create, "w");
    f.write(template);
    f.close();

    exit(0);


##----------------------------------------------------------------------------##
## Entry Point                                                                ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def main():
    args = sys.argv[1:]

    ##
    ## Parse the args...
    ## @todo(stdmatt): Add the dry run, silent/verbose args...
    if("--help" in args):
        show_help(0);
    if("--version" in args):
        show_version();
    if("--create" in args):
        create_dot_file();

    if(len(args) == 0):
        show_help(1);

    component_to_bump = find_component_to_bump(args);
    read_path         = find_dot_bump_fullpath(args);

    f = open(read_path);
    json_str = "".join(f.readlines());
    f.close();
    data = json.loads(json_str);

    for item in data:
        ## @todo(stdmatt): check about if the filepath
        ## should be relative to the .ark_bump or relative
        ## to the program execution...
        REGEX_VERSION="\d+\.\d+\.\d+";

        item_name     = item["name"];
        item_filename = item["file"];
        item_content  = item["content"];
        item_content  = ".*" + item_content.replace(" ", ".*") + ".*";
        item_content =  item_content.replace("_VERSION_", REGEX_VERSION);

        print("Doing task  [{}]".format(item_name))
        print("  File:     ({})".format(item_filename));
        print("  Content:  ({})".format(item_content));

        if(not os.path.isfile(item_filename)):
            log_fatal("File not found : ({0})", item_filename);

        f = open(item_filename);
        output = [];
        for line in f.readlines():
            x = re.search(item_content, line)
            if(x is None):
                output.append(line);
                continue;

            x = re.search(REGEX_VERSION, line);

            version     = x.group();
            new_version = bump(version, component_to_bump);

            new_line = line.replace(version, new_version);
            print("  Line:     ({})".format(line    .replace("\n", "")));
            print("  New Line: ({})".format(new_line.replace("\n", "")));
            output.append(new_line);
        f.close();

        f = open(item_filename, "w");
        f.writelines(output);
        f.close();

if __name__ == "__main__":
    main();
