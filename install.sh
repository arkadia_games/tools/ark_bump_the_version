#!/usr/bin/env bash
##
## Imports
##
source "$HOME/.ark/ark_shlib/main.sh"

##
## Constants
##
##------------------------------------------------------------------------------
## Script
SCRIPT_DIR=$(ark_get_script_dir);
## Program
PROGRAM_NAME="bump_the_version";
PROGRAM_INSTALL_NAME="bump-the-version";
PROGRAM_SOURCE_PATH="$SCRIPT_DIR/$PROGRAM_NAME";
PROGRAM_INSTALL_PATH="$HOME/.ark_bin";
PROGRAM_INSTALL_SUBPATH="$PROGRAM_INSTALL_PATH/$PROGRAM_NAME";

##
## Script
##
##------------------------------------------------------------------------------
echo "Installing ...";

## Create the install directory...
if [ ! -d "$PROGRAM_INSTALL_SUBPATH" ]; then
    echo "Creating directory at: ";
    echo "    $PROGRAM_INSTALL_SUBPATH";
    mkdir -p "$PROGRAM_INSTALL_SUBPATH" > /dev/null;
fi;

## Copy the file to the install dir...
cp -f "$PROGRAM_SOURCE_PATH/main.py" "$PROGRAM_INSTALL_SUBPATH/$PROGRAM_INSTALL_NAME";
chmod 744 "$PROGRAM_INSTALL_SUBPATH/$PROGRAM_INSTALL_NAME";

echo "$PROGRAM_INSTALL_NAME was installed at:";
echo "    $PROGRAM_INSTALL_SUBPATH";
echo "You might need add it to the PATH";
echo '    PATH=$PATH:'"$PROGRAM_INSTALL_SUBPATH"

echo "Done... ;D";
echo "";
